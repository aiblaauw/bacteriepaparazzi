# README #

Welcome to our project, which is based around the development of a plugin for the image-analysis program ImageJ/Fiji which. In this case the focus is on counting components in movies of reproducing, toxin-producing and sporulating *Bacillus cereus*. 

![shadow.png](https://bitbucket.org/repo/7bMBLk/images/1809943521-shadow.png)

The goal is to generate information from any given movie:

* Frequencies per frame (cells versus spores).
* Calculation of cell divisions and onset of sporulation.
* Duration until the last endospore is formed.
* Detection of fluorescence signal and calculation of percentage of subpopulation.
    
### How do I get set up? ###

* The first file (.jar) is what you can install in Fiji via Plugins>Install PlugIn...
* Then you need to store the other 2 files (.model and .arff) in the *Downloads* folder of your pc.

* Under **Edit>Options>Memory & Threads** you may edit settings to make sure Fiji askes the right amount of power from your PC. For example you can choose *3500 MB and 3 threads*.
* While opening dv files or other files with multiple channels from a microscope it is recommended to **split the channels** so you can use the spore and cell counting on the Brightfield channel movie and the GFP signal counting on the GFP channel movie. 
* Also it could be nice to use **Specify range for each series**, where you might want to use a *T Step range of 5 or 10 frames*, to reduce processing time of large movies on slower machines.
* Lastly, don’t forget to choose **Autoscale** to make sure the movie is shown correctly. 
The rest is up to you!

* You can find the functionality at **Analyze>Bacteria Analyzer**
* The output is written to the *Documents* folder of the pc, to a folder named *Bacteria Analyzer*; generated Excel tables, a TIF format copy of the used movie, the preprocessed movie and also a movie that is the result of the classifying.

### Usage ###

* **Preprocess** enhances the edges in the movie for classification and cell counting.
* **Classify** finds spores with help of the Weka trainable segmentation plugin, needs specific files in your Downloads folder. A movie with only black spots on a white background is the result. This process usually takes a while!
* **Count Spores** counts spores and generates a table with counts per frame. It works best on movies with prominent white spores that have sharp edges.
* **Count GFP** makes use of different filters on the GFP channel movie so that a table with counts per frame gets generated. This function works best on images with white cell-signals (which are not fused together that much) against a dark background with minimal noise. 
THIS IS NOT WORKING OPTIMAL IN THE CURRENT VERSION.
* **Count Cells** uses different filters on the preprocessed movie and generates a table with counts per frame is. This should work best for movies without a lot of noise and cells that are not fused together too much. 
THIS IS NOT WORKING OPTIMAL IN THE CURRENT VERSION.

### Download section? ###

* Bacterie_Paparazzi executable jar file
* Two segmentation files
* Two 2-frame test movies

### Dependencies? ###

The help of some programs/packages was needed to train data and find patterns,
they should all be found in the *lib* folder of this project.

* Fiji/ImageJ: http://fiji.sc/#download
* ij 1.50 jar from Fiji
* Weka 3.7.3 jar + trainable segmentation 3.1.0 jar + Imagelib2 2.6.0 jar 

### Who do I talk to? ###
Repo owner(s) or admin(s):

* Astrid Blaauw [aiblaauw@gmail.com]
* Elina Wever [e.e.wever@st.hanze.nl]

## Useful links ##

https://trello.com/b/u14RHCil/specialisatie-advanced-datamining-high-throughput-computing

http://fiji.sc/Introduction_into_Developing_Plugins#The_class_IJ

http://rsb.info.nih.gov/ij/developer/api/overview-summary.html

http://javadoc.imagej.net/Fiji/net/sf/ij/plugin/ImageIOSaveAsPlugin.html#ImageIOSaveAsPlugin%28%29

https://github.com/openmicroscopy/bioformats/blob/v5.1.7/components/bio-formats-plugins/utils/macros/batchTiffConvert.txt

http://fiji.sc/Nuclei_Watershed_Separation

http://imagej.1557.x6.nabble.com/jar-and-plugins-config-td5003514.html

Command Finder in Fiji for help when developing plugin