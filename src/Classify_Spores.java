/*
 * This plugin applies handmade classifier to a preprocessed movie.
 *
 * Copyright (C) 2016
 * Astrid Blaauw [aiblaauw@gmail.com]
 * Elina Wever [e.e.wever@st.hanze.nl]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.File;

//http://fiji.sc/Scripting_the_Trainable_Segmentation
//http://javadoc.imagej.net/Fiji/trainableSegmentation/WekaSegmentation.html
import trainableSegmentation.WekaSegmentation;

/**
 * This plugin applies handmade classifier to a preprocessed movie.
 *
 * @author eewever & aiblaauw
 */
public class Classify_Spores implements PlugInFilter {

    private ImagePlus ip;
    private String workingDir;
    private String downloadsDir;

    @Override
    public final int setup(final String string, final ImagePlus ip) {

        this.workingDir = Preprocess_Movie.workingDir;
        this.downloadsDir = System.getProperty("user.home") + File.separator + "Downloads" + File.separator;

        // open preprocessed file
        String inputPath = this.workingDir + "processed.tif";
        File f = new File(inputPath);
        // if preprocessed file does not exist, give error
        if (f.exists() && !f.isDirectory()) {
            this.ip = IJ.openImage(inputPath);
        } else {
            String lineSep = System.lineSeparator();
            IJ.error("Spore classifier",
                    "Preprocessed movie has to be available" + lineSep
                    + "Analyze>Bacteria Analyzer>Preprocess.");
            return DONE;
        }

        return PlugInFilter.DOES_8G;
    }

    @Override
    public final void run(final ImageProcessor imp) {
        this.classify(this.ip);
    }

    /**
     * Apply segmentation on preprocessed image. Results in classified 8-bit
     * image.
     *
     * @param ip image to work on
     */
    private void classify(final ImagePlus ip) {

        WekaSegmentation segmentator = new WekaSegmentation(ip);

        // load previously trained Weka classifier (.model) and data (.arff)
        File model = new File(this.downloadsDir + "spore_classifier.model");
        File arff = new File(this.downloadsDir + "spore_segmentation.arff");
        if (model.exists() && arff.exists()) {
            segmentator.loadClassifier(this.downloadsDir + "spore_classifier.model");
            segmentator.loadTrainingData(this.downloadsDir + "spore_segmentation.arff");
            // get result (labels float image)
            ImagePlus result = segmentator.applyClassifier(ip);
            // Display results
            IJ.saveAsTiff(result, this.workingDir + "count_spores");
        } else {
            String lineSep = System.lineSeparator();
            IJ.error("Spore classifier",
                    "Classifier files have to be available in your Downloads folder" + lineSep
                    + "Downloadable at: https://bitbucket.org/aiblaauw/bacteriepaparazzi");
        }
    }

}
