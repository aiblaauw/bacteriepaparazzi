/*
 * This plugin handles the movie-preprocessing for the bacterium recognition segmentation.
 *
 * Copyright (C) 2016
 * Astrid Blaauw [aiblaauw@gmail.com]
 * Elina Wever [e.e.wever@st.hanze.nl]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.WindowManager;
import ij.plugin.ImageCalculator;
import ij.plugin.Duplicator;
import ij.plugin.filter.PlugInFilter;
import ij.plugin.filter.RankFilters;
import ij.process.ImageProcessor;
import ij.process.ImageConverter;
import java.io.File;

/**
 * This plugin handles the movie-preprocessing for the bacterium recognition
 * segmentation.
 *
 * @author eewever & aiblaauw
 */
public class Preprocess_Movie implements PlugInFilter {

    private ImagePlus ip;
    private ImageProcessor imp;
    private ImagePlus image;
    private ImagePlus duplicatedImage;
    private final RankFilters filter;
    protected static String downloadsDir;
    protected static String workingDir;
    private String inputName;

    /**
     * Constructor for Bacterie_Paparazzi class.
     */
    public Preprocess_Movie() {

        this.filter = new RankFilters();
        Preprocess_Movie.downloadsDir = System.getProperty("user.home") + File.separator
                + "Documents" + File.separator + "Bacteria Analyzer" + File.separator;
    }

    /**
     * This method converts any opened file format to tiff and RGB format.
     *
     * @param image opened image in Imagej
     * @param directory to save file
     */
    public final void convertImage(final ImagePlus image, final String directory) {
        //call image converter  class
        ImageConverter ic = new ImageConverter(image);
        //convert image to 8-bit format
        ic.convertToGray8();
        image.updateAndDraw();
        //convert image to an .tiff file
        IJ.saveAs(image, "Tiff", directory);
    }

    /**
     * Overrides Fiji's setup command to start up the plugin. Returns help when
     * argument equals "about" and applies all filter settings of the run
     * function to all stacks. It only supports 8-bit files.
     *
     * @param arg arguments
     * @param ip ImagePlus object
     * @return needed file settings
     */
    @Override
    public final int setup(final String arg, final ImagePlus ip) {

        this.image = IJ.getImage();
        this.inputName = this.image.getShortTitle();
        Preprocess_Movie.workingDir = Preprocess_Movie.downloadsDir + inputName;
        new File(Preprocess_Movie.workingDir).mkdirs();
        Preprocess_Movie.workingDir = Preprocess_Movie.downloadsDir + inputName + File.separator;

        //convert image to an .tiff file and RGB format
        this.convertImage(this.image, Preprocess_Movie.workingDir + inputName);

        Duplicator duck = new Duplicator();
        this.duplicatedImage = duck.run(this.image);
        this.duplicatedImage.show();

        this.ip = ip;

        return PlugInFilter.DOES_8G | PlugInFilter.DOES_STACKS;

    }

    /**
     * Class overrides Fiji's run command to add information. It applies filter
     * settings for each .tiff file. Background noise is filtered out and edges
     * of each bacterium and spore is searched for.
     *
     * @param imp ImageProcessor object
     */
    @Override
    public final void run(final ImageProcessor imp) {

        //Filters background noise of each stackframe,
        //with imp being the image , the number the radius and the last one the method
        this.filter.rank(imp, 1.5, RankFilters.MEDIAN);
        this.filter.rank(imp, 3.0, RankFilters.MIN);

        ImageCalculator ic = new ImageCalculator();
        ImagePlus ip1 = WindowManager.getImage(this.duplicatedImage.getTitle());
        ImagePlus ip2 = WindowManager.getImage(this.image.getTitle());
        ImagePlus ip3 = ic.run("Subtract create stack", ip1, ip2);

        IJ.saveAsTiff(ip3, Preprocess_Movie.workingDir + "processed");
    }

}
