/*
 * This plugin is for counting spores and cells in microscopy movies!
 *
 * Copyright (C) 2016
 * Astrid Blaauw [aiblaauw@gmail.com]
 * Elina Wever [e.e.wever@st.hanze.nl]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * This plugin is for counting spores and cells in microscopy movies!
 *
 * @author eewever & aiblaauw
 * @version 0.1
 */
public class Bacterie_Paparazzi implements PlugInFilter {

    /**
     * Constructor for Bacterie_Paparazzi class.
     */
    public Bacterie_Paparazzi() {

    }

    /**
     * Contains information about this specific plugin. When user clicks on
     * Help>About plugins> Bacteria Analyzer... information about the plugin is
     * shown.
     *
     * @return about pop-up
     */
    public final String aboutPlugin() {
        //New line system dependant
        String lineSeparator = System.lineSeparator();
        String about
                = "Bacteria Analyzer, for counting spores and cells in microscopy images!"
                + lineSeparator + "By Elina Wever and Astrid Blaauw"
                + lineSeparator + "Institute for Life Science & Technology, Hanze University of Applied Sciences (NL)."
                + lineSeparator + "--"
                + lineSeparator + "This plugin contains kind of a workflow of buttons: "
                + lineSeparator + "-- Preprocess -- Enhances the edges in the movie for classification and cell counting."
                + lineSeparator + "-- Classify -- Finds spores with help of the Weka trainable segmentation plugin,"
                + lineSeparator + "needs specific files in your Downloads folder. * "
                + lineSeparator + "A movie with only black spots on a white background is the result. "
                + lineSeparator + "This process usually takes a while!"
                + lineSeparator + "-- Count Spores -- Counts spores and generates a table with counts per frame. "
                + lineSeparator + "It works best on movies with prominent white spores that have sharp edges."
                + lineSeparator + "-- Count GFP -- Makes use of different filters on the GFP channel movie"
                + lineSeparator + "so that a table with counts per frame gets generated."
                + lineSeparator + "This function works best on images with white cell-signals (which are not fused together that much)"
                + lineSeparator + "against a dark background with minimal noise. "
                + lineSeparator + "-- Count Cells -- Uses different filters on the preprocessed movie and "
                + lineSeparator + "generates a table with counts per frame is. "
                + lineSeparator + "This should work best for movies without a lot of noise and cells that are not fused together too much."
                + lineSeparator + "--"
                + lineSeparator + "The output is written to the Documents folder of the pc, to a folder named Bacteria Analyzer;"
                + lineSeparator + "generated Excel tables, a TIF format copy of the used movie, the preprocessed movie and "
                + lineSeparator + "also a movie that is the result of the classifying."
                + lineSeparator + "--"
                + lineSeparator + "Bitbucket: https://bitbucket.org/aiblaauw/bacteriepaparazzi"
                + lineSeparator + "* Here you can also find the needed classifier files.";
        IJ.showMessage(about);

        return about;
    }

    /**
     * Overrides Fiji's setup command to start up the plugin. Returns help when
     * argument equals "about" and applies all filter settings of the run
     * function to all stacks. It only supports 8-bit files.
     *
     * @param arg arguments
     * @param ip ImagePlus object
     * @return needed file formats
     */
    @Override
    public final int setup(final String arg, final ImagePlus ip) {

        if (arg.equals("about")) {
            // call function to show dialog.
            this.aboutPlugin();
            return PlugInFilter.DONE;
        }
        return PlugInFilter.DOES_ALL;
    }

    /**
     * Parsing Fiji "analyze particle" output table.
     *
     * @param inputPath string
     * @return arrayList, containing countings
     * @throws FileNotFoundException when input file not found
     * @throws IOException when outputreader doesn't respond
     */
    protected static ArrayList fetchOutput(final String inputPath)
            throws FileNotFoundException, IOException {
        ArrayList outputList = new ArrayList();
        BufferedReader outputReader = new BufferedReader(new FileReader(inputPath));
        String inputLine;
        while ((inputLine = outputReader.readLine()) != null) {
            String[] line = inputLine.split("\t");
            String count = line[1];
            outputList.add(count);
        }
        return outputList;
    }

    /**
     * Generates Excel file with countings.
     *
     * @param inputList with counts
     * @param type string, spores | gfp | cells
     * @param outputPath output table destination
     * @throws IOException when outputpath doesn't exist
     */
    protected static void writeOutput(final ArrayList inputList, final String type, final String outputPath)
            throws IOException {
        ArrayList outputList = new ArrayList();
        String header = "";
        if (type.equals("spores")) {
            header = "Frame\tSpores";
        }
        if (type.equals("gfp")) {
            header = "Frame\tGFP";
        }
        if (type.equals("cells")) {
            header = "Frame\tcells";
        }
        outputList.add(header);
        IntStream.range(1, inputList.size()).forEachOrdered(n -> {
            int slice = n;
            String count = inputList.get(n).toString();
            String outputLine = (slice + "\t" + count);
            outputList.add(outputLine);
        });
        Path file = Paths.get(outputPath);
        Files.write(file, outputList, Charset.forName("UTF-8"));
    }

    /**
     * Class overrides Fiji's run command to add information. It applies filter
     * settings for each .tiff file. Background noise is filtered out and edges
     * of each bacterium and spore is searched for.
     *
     * @param imp ImageProcessor object
     */
    @Override
    public void run(final ImageProcessor imp) {
        // Place for specific commands and more control.
        // See ImageJ documentation for more info.
    }

}
