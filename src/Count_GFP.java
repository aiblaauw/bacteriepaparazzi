/*
 * This plugin generates a table with GFP-signaling cell countings.
 *
 * Copyright (C) 2016
 * Astrid Blaauw [aiblaauw@gmail.com]
 * Elina Wever [e.e.wever@st.hanze.nl]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.Duplicator;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eewever & aiblaauw
 */
public class Count_GFP implements PlugInFilter {

    private ImagePlus ip;
    private String outputpath;
    private String workingDir;
    private ImagePlus duplicatedImage;
    private String inputName;

    @Override
    public final int setup(final String string, final ImagePlus ip) {

        this.workingDir = Preprocess_Movie.workingDir;
        this.ip = ip;

        if (this.ip == null) {
            IJ.error("No Image",
                    "There are no images open.");
            return DONE;
        }

        this.inputName = IJ.getImage().getShortTitle();

        Duplicator duck = new Duplicator();
        this.duplicatedImage = duck.run(this.ip);
        this.duplicatedImage.show();

        this.ip = this.duplicatedImage;

        ImageConverter ic = new ImageConverter(this.ip);
        ic.convertToGray8();
        this.ip.updateAndDraw();

        // enhancing cells and reducing noise
        IJ.run(this.ip, "Median...", "radius=2 stack");
        IJ.run(this.ip, "Smooth", "stack");
        IJ.run(this.ip, "Subtract Background...", "rolling=4 sliding stack");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Invert", "stack");
        IJ.run(this.ip, "Remove Outliers...", "radius=2 threshold=50 which=Bright stack");
        IJ.run(this.ip, "Find Edges", "stack");
        IJ.run(this.ip, "Make Binary", "method=Otsu background=Light calculate");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Remove Outliers...", "radius=1.5 threshold=50 which=Bright stack");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Analyze Particles...", "exclude summarize stack");

        // save table
        this.ip = ip;
        if (this.workingDir != null) {
            // if preprocess command was executed earlier so the workingdir is known
            this.outputpath = Preprocess_Movie.workingDir + "count_gfp.xls";
            IJ.saveAs("Results", this.outputpath);
        } else {
            // if the preprocess command was not executed earlier so the workingdir has to be created here
            String workingDirTest = System.getProperty("user.home") + File.separator
                    + "Documents" + File.separator + "Bacteria Analyzer" + File.separator;
            String workingDirUpdate = workingDirTest + this.inputName;
            new File(workingDirUpdate).mkdirs();
            this.workingDir = workingDirUpdate + File.separator;
            this.outputpath = this.workingDir + "count_gfp.xls";
            IJ.saveAs("Results", this.outputpath);
        }

        try {
            // parse output
            ArrayList sporesOutput = Bacterie_Paparazzi.fetchOutput(this.outputpath);
            Bacterie_Paparazzi.writeOutput(sporesOutput, "gfp", this.outputpath);
        } catch (IOException ex) {
            Logger.getLogger(Count_Spores.class.getName()).log(Level.SEVERE, null, ex);
        }

        return PlugInFilter.DOES_ALL;
    }

    @Override
    public final void run(final ImageProcessor ip) {
        // Place for specific commands and more control.
        // See ImageJ documentation for more info.
    }

}
