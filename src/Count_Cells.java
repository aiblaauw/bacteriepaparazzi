/*
 * This plugin generates a table with cell countings.
 *
 * Copyright (C) 2016
 * Astrid Blaauw [aiblaauw@gmail.com]
 * Elina Wever [e.e.wever@st.hanze.nl]
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

import ij.IJ;
import ij.ImagePlus;
import ij.plugin.filter.PlugInFilter;
import ij.process.ImageProcessor;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author eewever & aiblaauw
 */
public class Count_Cells implements PlugInFilter {

    private ImagePlus ip;
    private String workingDir;
    private String outputpath;

    @Override
    public final int setup(final String string, final ImagePlus ip) {

        this.workingDir = Preprocess_Movie.workingDir;

        // open preprocessed file
        String inputPath = this.workingDir + "processed.tif";
        File f = new File(inputPath);
        // if preprocessed file does not exist, give error
        if (f.exists() && !f.isDirectory()) {
            this.ip = IJ.openImage(inputPath);
        } else {
            String lineSep = System.lineSeparator();
            IJ.error("Cell counter",
                    "Preprocessed movie has to be available" + lineSep
                    + "Analyze>Bacteria Analyzer>Preprocess.");
            return DONE;
        }

        // enhancing cells and reducing noise
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Make Binary", "method=Otsu background=Dark calculate");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Maximum...", "radius=2.5 stack");
        IJ.run(this.ip, "Invert", "stack");
        IJ.run(this.ip, "Despeckle", "stack");
        IJ.run(this.ip, "Remove Outliers...", "radius=3 threshold=50 which=Dark stack");
        IJ.run(this.ip, "Remove Outliers...", "radius=1.5 threshold=50 which=Bright stack");
        IJ.run(this.ip, "Minimum...", "radius=2 stack");
        IJ.run(this.ip, "Remove Outliers...", "radius=3 threshold=50 which=Dark stack");
        // count particles!
        IJ.run(this.ip, "Analyze Particles...", "exclude summarize stack");
        // save table
        this.outputpath = this.workingDir + "count_cells.xls";
        IJ.saveAs("Results", this.outputpath);

        try {
            // parse output
            ArrayList sporesOutput = Bacterie_Paparazzi.fetchOutput(this.outputpath);
            Bacterie_Paparazzi.writeOutput(sporesOutput, "cells", this.outputpath);
        } catch (IOException ex) {
            Logger.getLogger(Count_Spores.class.getName()).log(Level.SEVERE, null, ex);
        }

        return PlugInFilter.DOES_ALL;
    }

    @Override
    public final void run(final ImageProcessor ip) {
        // Place for specific commands and more control.
        // See ImageJ documentation for more info.
    }

}
